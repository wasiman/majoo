import 'package:flutter/material.dart';
import 'package:majoo/providers/mainprovider.dart';
import 'package:majoo/screens/home/widgetfavorite.dart';
import 'package:majoo/screens/home/widgethome.dart';
import 'package:majoo/screens/home/widgetprofile.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static const List<Widget> _widgetOptions = <Widget>[
    WidgetHome(),
    WidgetFavorite(),
    WidgetProfile()
  ];

  @override
  Widget build(BuildContext context) {
    final _p = Provider.of<MainProvider>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Majoo People'),
        actions: [
          TextButton(
              onPressed: () {
                setState(() {
                  _p.setviewmode();
                });
              },
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
              child: _p.viewmode
                  ? const Icon(Icons.list)
                  : const Icon(Icons.grid_3x3))
        ],
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Beranda'),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite), label: 'Favorit'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile')
          ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () => ambildata(),
        child: const Icon(Icons.download),
      ),
    );
  }

  ambildata() {
    final _p = Provider.of<MainProvider>(context, listen: false);
    _p.getpeople();
  }
}
