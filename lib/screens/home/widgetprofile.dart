import 'package:flutter/material.dart';
import 'package:majoo/providers/mainprovider.dart';
import 'package:majoo/screens/launchscreen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WidgetProfile extends StatelessWidget {
  const WidgetProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _p = Provider.of<MainProvider>(context, listen: true);
    _p.getprofile();
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Column(
          children: [
            const CircleAvatar(
              radius: 50,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Text(
                _p.profile,
                style: const TextStyle(fontSize: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: TextButton(
                onPressed: () async {
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.clear();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const LaunchScreen(),
                      ));
                },
                child: const Text("Logout"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
