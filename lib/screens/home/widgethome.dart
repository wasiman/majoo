import 'package:flutter/material.dart';
import 'package:majoo/models/responsemodel.dart';
import 'package:majoo/providers/mainprovider.dart';
import 'package:provider/provider.dart';

class WidgetHome extends StatelessWidget {
  const WidgetHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _p = Provider.of<MainProvider>(context, listen: true);
    return _p.viewmode ? listmode(context) : gridmode(context);
  }

  Widget listmode(context) {
    return FutureBuilder<ResponseModel>(
      future: Provider.of<MainProvider>(context, listen: false).people,
      builder: (context, snapshot) => snapshot.hasData
          ? ListView.builder(
              itemCount: snapshot.data?.data.length ?? 0,
              itemBuilder: (context, index) => ListTile(
                leading: CircleAvatar(
                  radius: 30.0,
                  backgroundImage:
                      NetworkImage("${snapshot.data!.data[index]['avatar']}"),
                  backgroundColor: Colors.transparent,
                ),
                subtitle: Text(snapshot.data!.data[index]['email']),
                title: Text(snapshot.data!.data[index]['first_name'] +
                    " " +
                    snapshot.data!.data[index]['first_name']),
              ),
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget gridmode(context) {
    final orientation = MediaQuery.of(context).orientation;
    return FutureBuilder<ResponseModel>(
      future: Provider.of<MainProvider>(context, listen: false).people,
      builder: (context, snapshot) => snapshot.hasData
          ? GridView.builder(
              itemCount: snapshot.data!.data.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount:
                      (orientation == Orientation.portrait) ? 2 : 3),
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: Column(
                    children: [
                      GridTile(
                          child: CircleAvatar(
                        radius: 60,
                        backgroundImage: NetworkImage(
                            "${snapshot.data!.data[index]['avatar']}"),
                        backgroundColor: Colors.transparent,
                      )),
                      Text(snapshot.data!.data[index]['first_name'] +
                          " " +
                          snapshot.data!.data[index]['first_name']),
                      Text(
                        snapshot.data!.data[index]['email'],
                        style: const TextStyle(fontSize: 12),
                      )
                    ],
                  ),
                );
              },
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
