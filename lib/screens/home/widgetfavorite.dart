import 'package:flutter/material.dart';

class WidgetFavorite extends StatelessWidget {
  const WidgetFavorite({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text("Favorite"),
    );
  }
}
