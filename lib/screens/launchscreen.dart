import 'package:flutter/material.dart';
import 'package:majoo/providers/mainprovider.dart';
import 'package:majoo/screens/auth/loginscreen.dart';
import 'package:majoo/screens/home/homescreen.dart';
import 'package:provider/provider.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  @override
  State<LaunchScreen> createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _p = Provider.of<MainProvider>(context, listen: false);
    return FutureBuilder<bool>(
      future: _p.islogin(),
      builder: (context, snapshot) => snapshot.hasData
          ? snapshot.data == true
              ? const HomeScreen()
              : const LoginScreen()
          : const LoginScreen(),
    );
  }
}
