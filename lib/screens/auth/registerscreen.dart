import 'package:flutter/material.dart';
import 'package:majoo/helpers/dbuser.dart';
import 'package:majoo/models/usermodel.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController namaController = TextEditingController();
  TextEditingController sandiController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
            onPressed: () => submit(),
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            ),
            child: const Icon(Icons.save),
          )
        ],
        title: const Text("Register"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                validator: (value) {
                  return value ?? "Isikan nama";
                },
                controller: namaController,
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: "Nama",
                    hintText: "Silahkan isi nama"),
              ),
              TextFormField(
                validator: (value) {
                  return value ?? "Isikan sandi";
                },
                controller: sandiController,
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(
                    icon: Icon(Icons.lock),
                    labelText: "Sandi",
                    hintText: "Silahkan isi sandi"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  submit() async {
    var _data =
        UserModel(nama: namaController.text, sandi: sandiController.text);
    await DbUser().insert(_data).then((value) => Navigator.pop(context));
  }
}
