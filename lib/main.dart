import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/mainprovider.dart';
import 'screens/launchscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => MainProvider(),
        child: MaterialApp(
          title: 'MAJOO',
          initialRoute: '/',
          routes: {
            '/': (context) => const LaunchScreen(),
          },
        ));
  }
}
