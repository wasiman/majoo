import 'package:dio/dio.dart';
import 'package:majoo/models/responsemodel.dart';

class PeopleProvider {
  Future<ResponseModel> teams() async {
    try {
      Response response = await Dio().get('https://reqres.in/api/users');
      return ResponseModel.fromJson(response.data);
    } on DioError catch (e) {
      return e.response?.data;
    }
  }
}
