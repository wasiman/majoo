import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majoo/helpers/dbuser.dart';
import 'package:majoo/models/responsemodel.dart';
import 'package:majoo/providers/peopleprovider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainProvider with ChangeNotifier {
  String profile = "";
  bool viewmode = true;

  Future<ResponseModel>? people;

  Future<bool> islogin() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey("islogin");
  }

  getprofile() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    int? _id = prefs.getInt('userid');
    profile = await DbUser().selectid(_id!).then((value) => value[0].nama);
    notifyListeners();
  }

  setviewmode() async {
    viewmode = viewmode ? false : true;
    notifyListeners();
  }

  getpeople() async {
    var _people = PeopleProvider();
    people = _people.teams();
    // people.then((value) {
    //   if (value.data.isNotEmpty) {
    //     DbPeople().deleteall().then((x) => DbPeople().insert(PeopleModel(
    //         firstname: value.data[0]['firstname'],
    //         lastname: value.data[0]['lastname'],
    //         avatar: value.data[0]['avatar'],
    //         email: value.data[0]['email'])));
    //   }
    // });
    notifyListeners();
  }
}
