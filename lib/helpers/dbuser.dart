import 'dart:async';
import 'package:majoo/models/usermodel.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbUser {
  Future<Database> initDB() async {
    String databasesPath = await getDatabasesPath();
    String dbPath = join(databasesPath, 'majoo.db');
    return openDatabase(dbPath, version: 1, onCreate: populateDb);
  }

  void populateDb(Database database, int version) async {
    await database.execute("CREATE TABLE user ("
        "id INTEGER PRIMARY KEY,"
        "nama TEXT,"
        "sandi TEXT"
        ")");
  }

  Future<void> insert(UserModel user) async {
    final db = await initDB();
    await db.insert(
      'user',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<UserModel>> select() async {
    final db = await initDB();
    final List<Map<String, dynamic>> maps = await db.query('user');
    return List.generate(maps.length, (i) {
      return UserModel(
        id: maps[i]['id'],
        nama: maps[i]['nama'],
        sandi: maps[i]['sandi'],
      );
    });
  }

  Future<List<UserModel>> selectnama(String _nama) async {
    final db = await initDB();
    final List<Map<String, dynamic>> maps =
        await db.query('user', where: 'nama = ?', whereArgs: [_nama]);
    return List.generate(maps.length, (i) {
      return UserModel(
        id: maps[i]['id'],
        nama: maps[i]['nama'],
        sandi: maps[i]['sandi'],
      );
    });
  }

  Future<List<UserModel>> selectid(int _id) async {
    final db = await initDB();
    final List<Map<String, dynamic>> maps =
        await db.query('user', where: 'id = ?', whereArgs: [_id]);
    return List.generate(maps.length, (i) {
      return UserModel(
        id: maps[i]['id'],
        nama: maps[i]['nama'],
        sandi: maps[i]['sandi'],
      );
    });
  }

  Future<void> update(UserModel user) async {
    final db = await initDB();
    await db.update(
      'user',
      user.toMap(),
      where: 'id = ?',
      whereArgs: [user.id],
    );
  }

  Future<void> delete(int id) async {
    final db = await initDB();
    await db.delete(
      'user',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
