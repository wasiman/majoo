import 'package:flutter/material.dart';

class AppHelper {
  Future setPesan(pesan, title, context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(pesan ?? "MAJOO!"),
            actions: <Widget>[
              ElevatedButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                ),
                child: const Text('Close'),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }
}
