import 'dart:async';
import 'package:majoo/models/peoplemodel.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbPeople {
  Future<Database> initDB() async {
    String databasesPath = await getDatabasesPath();
    String dbPath = join(databasesPath, 'majoo.db');
    return openDatabase(dbPath, version: 1, onCreate: populateDb);
  }

  void populateDb(Database database, int version) async {
    await database.execute("CREATE TABLE people ("
        "id INTEGER PRIMARY KEY,"
        "email TEXT,"
        "first_name TEXT,"
        "last_name TEXT,"
        "avatar TEXT,"
        ")");
  }

  Future<void> insert(PeopleModel people) async {
    final db = await initDB();
    await db.insert(
      'people',
      people.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<PeopleModel>> select() async {
    final db = await initDB();
    final List<Map<String, dynamic>> maps = await db.query('people');
    return List.generate(maps.length, (i) {
      return PeopleModel(
        id: maps[i]['id'],
        email: maps[i]['email'],
        firstname: maps[i]['firstname'],
        lastname: maps[i]['lastname'],
        avatar: maps[i]['avatar'],
      );
    });
  }

  Future<List<PeopleModel>> selectid(int _id) async {
    final db = await initDB();
    final List<Map<String, dynamic>> maps =
        await db.query('people', where: 'id = ?', whereArgs: [_id]);
    return List.generate(maps.length, (i) {
      return PeopleModel(
        id: maps[i]['id'],
        email: maps[i]['email'],
        firstname: maps[i]['firstname'],
        lastname: maps[i]['lastname'],
        avatar: maps[i]['avatar'],
      );
    });
  }

  Future<void> update(PeopleModel people) async {
    final db = await initDB();
    await db.update(
      'people',
      people.toMap(),
      where: 'id = ?',
      whereArgs: [people.id],
    );
  }

  Future<void> delete(int id) async {
    final db = await initDB();
    await db.delete(
      'people',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<void> deleteall() async {
    Database db = await initDB();
    await db.rawDelete("Delete from people");
  }
}
