class PeopleModel {
  final int? id;
  final String email;
  final String firstname;
  final String lastname;
  final String avatar;

  PeopleModel(
      {this.id,
      required this.firstname,
      required this.lastname,
      required this.avatar,
      required this.email});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'email': email,
      'firstname': firstname,
      'lastname': lastname,
      'avatar': avatar,
    };
  }
}
