class UserModel {
  final int? id;
  final String nama;
  final String sandi;

  UserModel({this.id, required this.nama, required this.sandi});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nama': nama,
      'sandi': sandi,
    };
  }
}
